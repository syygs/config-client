package org.gs.cloud;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class Test1Controller {
	@Value("${from}")
	private String from;
	
	@RequestMapping("/from1")
	public String from(){
		return this.from;
	}
}
